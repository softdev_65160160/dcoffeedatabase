/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.dao.UserDao;
import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author Lenovo
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for(User u:userDao.getAll()){
            System.out.println(u);
        }
//        User user1 = userDao.get(2);
//        System.out.println(user1);
//        
//        //User newUser = new User("user3","password",2,"F"); 
//        
//        User insertedUser = userDao.get(4);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M");
//        userDao.update(insertedUser);
//        User updateUser = userDao.get(insertedUser.getId());
//        System.out.println(updateUser);
//        
//        userDao.delete(user1);
        for(User u:userDao.getAll()){
            System.out.println(u);
        }
        
        for(User u:userDao.getAllOrderBy("user_name", "asc")){
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
